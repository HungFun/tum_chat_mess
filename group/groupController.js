import mongoose from "mongoose";
import Group from "./group"
const CONSTANT = require('../contants/contant')
const Response = require('../utils/response')
const Mess = require('../model/message')

export const createRoom = async (req, res, next) => {
    let group = new Group({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        type: req.body.type,
        onwer: req.body.onwer,
        idFriendChat: req.body.idFriendChat,
        avatarFriendChat: req.body.avatarFriendChat
    })
    try {

        const findGroupByIdOnwer = await Group.find({
            onwer: req.body.onwer,
            type: "ONE",
            idFriendChat: req.body.idFriendChat[0]
        });
        console.log(findGroupByIdOnwer);
        const findGroupByidFriendChat = await Group.find({
            onwer: req.body.idFriendChat[0],
            type: "ONE",
            idFriendChat: req.body.onwer
        });

        if (findGroupByIdOnwer.length > 0 || findGroupByidFriendChat.length > 0) {
            res.send(new Response(true, CONSTANT.GROUP_EXIST, null))
        }
        const response = await group.save()
        if (response) {
        
            res.send(new Response(false, CONSTANT.CREATE_ROOM_SUCCESS, response))

        }
    } catch (err) {
        res.send(new Response(true, CONSTANT.CREATE_ROOM_FALSE, null))

    }
}

export const findAllGroup = async (req, res, next) => {
    try {
        const response = await Group.find({
            $or: [
                { onwer: req.body._id },
                { idFriendChat:{ $all:[req.body._id]} }
            ]
        });

        if (response) {
            return res.json({
                data: response
            });
        }
    } catch (err) {
        return res.json({
            data: err
        });
    }
}

export const deleteGroup = async (req, res, next) => {
    try {
        const id = req.params.id;
        console.log(id);
        const deleteGroup = await Group.deleteOne({
            _id: id
        })
        // const deleteMess = await Mess.remove({
        //     room: { $gte: id }
        // })
        
        if (deleteGroup) {
            res.send(new Response(false, CONSTANT.DELETE_ROOM_SUCCESS, null))
        }
    } catch (err) {
        return res.json({

            data: err
        });
    }
}



