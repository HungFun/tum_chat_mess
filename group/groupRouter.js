import express from "express";
const router = express.Router();
import * as groupController from "./groupController"

router.post("/api/allGroup",groupController.findAllGroup)

router.post("/api/createRoom",groupController.createRoom)

router.delete("/api/deleteRoom:id",groupController.deleteGroup)


export default router;
