const mongoose = require("mongoose");

const groupSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,

    name: {
        type: String
    },
    type: {
        type: String
    },
    onwer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
    },
    // user
    idFriendChat: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User",
        }
    ],
    avatarFriendChat: {
        type: String
    }
});

export default mongoose.model("Group", groupSchema);
