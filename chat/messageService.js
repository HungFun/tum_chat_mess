import mongoose from "mongoose";
import Message from "../model/message"


const addMessage = async ({content, name, room, type, image}) => {
    name = name.trim().toLowerCase();
    room = room.trim().toLowerCase();

    let newMessage = new Message({
        _id: new mongoose.Types.ObjectId(),
        time: Date.now(),
        type: type,
        content: content,
        room: room,
        username: name,
        image: image
    })

    try {
        return await newMessage.save();
    }catch (err){
        console.log(err)
    }

}

const getMessageByRoom = async ({room}) => {
    room = room.trim().toLowerCase();
    try {
        const messItem = await Message.find({
            room: room
        })

        return messItem
    }catch (err) {
        console.log(err)
    }
}

module.exports = {addMessage, getMessageByRoom};
