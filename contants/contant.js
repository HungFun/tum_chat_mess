

// Create room
module.exports.CREATE_ROOM_SUCCESS = 'Tạo nhóm thành công'
module.exports.CREATE_ROOM_FALSE = 'Tạo nhóm thất bại'
module.exports.GROUP_EXIST = 'Group đã tồn tại'

// Delete room
module.exports.DELETE_ROOM_SUCCESS = 'Xóa room thành công'
module.exports.DELETE_ROOM_FALSE  = 'Xóa room thất bại'
module.exports.DELETE_MESS_SUCCESS = 'Xóa tin nhắn thành công'
module.exports.DELETE_MESS_FALSE  = 'xÓA tin nhắn thất bại'

