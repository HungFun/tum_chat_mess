const { addUser, removeUser, getUser, getUsersInRoom } = require('./chat/user');
var express = require("express")();
import expressTool from "express";
var http = require('http').createServer(express)
var io = require('socket.io')(http);
import bodyParser from "body-parser";
import mongoose from "mongoose";
import GroupRouter from './group/groupRouter'

const { addMessage, getMessageByRoom } = require('./chat/messageService')

express.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
})

express.use(bodyParser.urlencoded({ extended: false }))
express.use(bodyParser.json())

express.get('/', (req, res) => {
    // websocket dùng để chat
})
express.post('/', (req, res, next) => {
    console.log(req.body.phone);
    res.status(200).json({ message: 'ok' });
})

express.get('/123', (req, res, next) => {
    res.status(200).json({ message: 'ok' });
})

express.get('/message/:id', (req, res, next) => {
    const id = req.params.id;
    console.log(id)
    const aaa = getMessageByRoom({ room: id }).then(r => {
        let listUser = []
        r.forEach(mess => {
            const item = {
                _id: mess._id,
                content: mess.content,
                room: mess.room,
                type: mess.type,
                time: mess.time,
                image: mess.image,
                user: {
                    email: mess.username,
                    phone: mess.username
                }
            }
            listUser.push(item)

        })
        res.status(200).json({
            status: 'success',
            message: 'Thành công',
            data: listUser
        });


    }).catch(err => {
        res.status(500).json({
            status: 'fail',
            message: 'thất bại',
            data: err
        });
    })


})
// gọi tới api group
express.use("/group", GroupRouter);

// hệ thống chạy port 3007
http.listen(3003, function (err) {
    if (!err)
        console.log("Server is staring...");
    else console.log(err)

});

mongoose.connect(
    "mongodb+srv://DoxuanHung:729199dohung@cluster0.pnryf.mongodb.net/TumblrzData?retryWrites=true&w=majority",
    { useCreateIndex: true, useFindAndModify: false, useUnifiedTopology: true },
    function (err, db) {
        if (err) throw err;
        console.log("Ket noi thanh cong");
    }
);

io.on('connect', (socket) => {
    console.log("đã kết nối vào socket")
    // hiện thông tin socket đã kết nối vào
    console.log(socket.id)
    // khi vào mới vào cần phải vào 'join' để lưu thông tin user để biết user tên gì và đang chat ở phòng nào
    // bắt buộc lưu để user xuống
    socket.on('join', ({ name, room }, callback) => {
        console.log("aaaa");
        console.log(name)

        // add user có cùng phòng vào 1 mảng

        if (name === undefined || room === undefined) {
            console.log("bị underfinde check lại")
            socket.emit('message', { user: 'admin', text: `error` });
            callback();
        }
        else {
            const checkUser = getUser(socket.id);
            if (checkUser) {
                removeUser(socket.id);
            }

            console.log("đây là tên")
            console.log("đây là room")
            console.log(room)
            const { error, user } = addUser({ id: socket.id, name, room });
            if (error) return callback(error);

            socket.join(user.room);
            // gửi message cho những người dùng có trong hệ thống khi có ng mới vào

            io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room) });
            callback();
        }
    });

    // dùng để gửi tin nhắn vào nhóm mình đang có mặt
    // khi muốn gửi bắt sự kiện 'sendMessage' để gửi xuống
   socket.on('sendMessage', ({text,type,image},callback) => {
        console.log(text)
        try {
            const user = getUser(socket.id);
            console.log("///////")
            console.log(user)
            addMessage({content: text, name: user.name, room: user.room, type: type, image: image}).then(r => {
                console.log("đã thêm tin nhắn")
                //console.log(r)
            })
            // khi dùng thì luôn để socket bật với message để bắt sự kiện gửi tin nhắn từ các user khác
            io.to(user.room).emit('message', {user: user.name, text: text, type: type, image: image});
        }catch (err) {
            console.log(err)
        }
        callback();
    });
   

    // gửi room xuống để chat
    // thông tin sẽ dc gửi tuần tự từ trên xuống
    socket.on('loadMessage', async (room, callback) => {
        const user = getUser(socket.id);
        console.log(user)
        getMessageByRoom({ room: room }).then(r => {
            console.log(r.length)
            r.forEach(x => {
                io.to(user.id).emit('message', { user: x.username, text: x.content, time: x.time });
            })

        })

        callback();
    });

    // khi muốn thoát khỏi nhóm thì dùng 'disconnect'
    socket.on('disconnect', () => {
        const user = removeUser(socket.id);
        console.log("kick khỏi group")
    })
});
