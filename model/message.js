const mongoose = require("mongoose");

const messageSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,

    content: {
        type: String,
        default: "",
    },
    time: {
        type: Date
    },

    type: {
        type: String
    },

    room: {
        type: String,
        default: "",
        require: true
    },
    username: {
        type: String,
        default: "",
        require: true
    },
    image: {
        type: String,
    }
})

export default mongoose.model("Message", messageSchema);

